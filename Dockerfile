# --------------------------------------------------------------------
# Base stage for getting all the dependencies installed (dev and prod)
# --------------------------------------------------------------------
FROM python:3.8.0-alpine as install-deps
COPY . /code
WORKDIR /code
RUN apk add --no-cache build-base \
	&& make cleanup \
	&& export PIP_NO_CACHE_DIR=1 PIP_DISABLE_PIP_VERSION_CHECK=1 \
	&& pip install poetry==0.12.17 \
	&& poetry config settings.virtualenvs.create false \
	&& poetry install \
	&& rm -rf .git \
	&& apk del --purge build-base;


# ----------------------------------------
# Build stage specifically for development
# ----------------------------------------
FROM python:3.8.0-alpine as dev
COPY --from=install-deps /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=install-deps /usr/local/bin /usr/local/bin
ENV PYTHONASYNCIODEBUG=1
RUN apk add --no-cache make
WORKDIR /code


# -----------------------------------------------
# Build stage specifically for the CI environment
# -----------------------------------------------
FROM dev as ci
COPY --from=install-deps /code /code
ENTRYPOINT ["script/cibuild"]


# ------------------------------------------------------------------
# Build stage dedicated to cleaning up and installing for production
# ------------------------------------------------------------------
# - build the dist wheel
# - uninstall all dev dependencies
# - uninstall vengeance from editable mode
# - install vengeance officially
# - uninstall poetry
FROM ci as build-for-prod
ENV PYTHONASYNCIODEBUG=0
WORKDIR /code
RUN poetry build -f wheel \
	&& poetry install --no-dev \
	&& pip uninstall vengeance -y \
	&& pip install dist/vengeance-0.1.0-py3-none-any.whl \
	&& pip uninstall poetry -y \
	&& rm -rf /code;


# --------------------------------------------------
# Copy files built for production into a fresh image
# --------------------------------------------------
FROM python:3.8.0-alpine as prod
COPY --from=build-for-prod /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=build-for-prod /usr/local/bin /usr/local/bin
ENTRYPOINT ["vengeance"]
