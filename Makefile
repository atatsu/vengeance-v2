.PHONY: cleanup test-continuous test lint type-check

cleanup:
	rm -rf .mypy_cache \
		&& rm -rf .pytest_cache \
		&& rm -rf virtualenv \
		&& rm -rf .eggs \
		&& rm -rf dist \
		&& rm -rf *.egg-info \
		&& find . -name "__pycache__" -type d -exec rm -rf {} +;

test-continuous:
	PYTHONASYNCIODEBUG=1 ptw . --afterrun 'make lint && make type-check' -- -vv;

type-check:
	mypy vengeance;

lint:
	flake8 vengeance && flake8 tests;

test:
	PYTHONASYNCIODEBUG=1 python -m pytest -vv;
